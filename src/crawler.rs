use std::collections::HashMap;
use std::collections::HashSet;

use regex::Regex;
use url::Url;

use crate::errors::*;
use crate::persistence::*;

///! Crawls the given base url.
pub fn crawl(base_url: Url) -> Result<CrawlResult> {
    // Queue of paths within this domain to visit
    let mut queue = vec![String::from("/")];
    // Where internal links are stored
    let mut internal = HashMap::new();
    // Where external links are stored
    let mut external = HashSet::new();

    // HTTP client to send requests with
    let client = reqwest::Client::new();

    while let Some(next_path) = queue.pop() {
        let mut next_url = base_url.clone();
        next_url.set_path(&next_path);

        // Request next page
        let response = client.get(next_url).send()?.text()?;

        // Parse links in HTML page
        let urls = html_links(&response).flat_map(|link| {
                       Url::parse(&link).or(base_url.join(&link)).ok()
                   });

        for url in urls {
            // Triage internal and external links
            if url.host() == base_url.host() {
                let path = String::from(url.path());
                let count = internal.entry(path.clone()).or_insert(0);
                if *count == 0 {
                    // Visit internal links which we don't encountered
                    queue.push(path);
                }
                *count += 1;
            } else {
                external.insert(url);
            }
        }
    }

    // Collect internal links
    let mut internal_links = internal.drain()
                                     .map(|(path, _count)| path)
                                     .collect::<Vec<String>>();
    internal_links.sort();

    // Collect external links
    let mut external_links = external.drain()
                                     .map(|url| String::from(url.as_str()))
                                     .collect::<Vec<String>>();
    external_links.sort();

    let domain = base_url.host_str()
                         .ok_or(ErrorKind::NoDomain(base_url.clone()))?;

    Ok(CrawlResult { domain: String::from(domain),
                     internal: internal_links,
                     external: external_links })
}

///! Returns an iterator over the links found in the given HTML page
fn html_links<'a>(html: &'a str) -> impl Iterator<Item = &'a str> {
    lazy_static! {
        static ref ANCHOR_MATCH: Regex =
            Regex::new("<a [^>]*href=(\"([^\"]*)\"|\'([^\']*)\')").unwrap();
    }

    ANCHOR_MATCH.captures_iter(html).map(|capture| {
                                        capture.get(2)
                                               .or(capture.get(3))
                                               .unwrap()
                                               .as_str()
                                    })
}


#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_html_links() {
        let html = r#"
        <a id="test" class="test" href="example.com"></a>
        <a id="test" class="test" href='example.com'></a>
        <a id="test" class="test" href="https://example.com"></a>
        <a id="test" class="test" href="../../index.html"></a>
        <a id="test" class="test" href="https://example.com/'link'"></a>
        <a id="test" class="test" href='https://example.com/"link"'></a>
        "#;

        let mut links = html_links(html).collect::<Vec<&str>>();
        links.reverse();

        dbg!(&links);
        assert_eq!(links.pop(), Some("example.com"));
        assert_eq!(links.pop(), Some("example.com"));
        assert_eq!(links.pop(), Some("https://example.com"));
        assert_eq!(links.pop(), Some("../../index.html"));
        assert_eq!(links.pop(), Some("https://example.com/\'link\'"));
        assert_eq!(links.pop(), Some("https://example.com/\"link\""));
        assert_eq!(links.pop(), None);
    }


}
