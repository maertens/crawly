use rocket::http::ContentType;
use rocket::http::Status;
use rocket::request::Request;
use rocket::response;
use rocket::response::Responder;
use rocket::Response;
use std::io::Cursor;
use url::Url;

error_chain! {
    foreign_links {
        Reqwest(reqwest::Error) #[doc = "Error propagated from Reqwest"];
        Sled(sled::Error) #[doc = "Error propagated from Sled"];
        IO(std::io::Error) #[doc = "IO error"];
        Bincode(bincode::Error) #[doc = "bincode error"];
    }
    errors {
        /// An url was given without domain
        NoDomain(url: Url) {
            description("URL given without domain")
            display("URL given without domain: {}", url)
        }
    }
}

impl<'r> Responder<'r> for Error {
    fn respond_to(self, _: &Request) -> response::Result<'r> {
        let json = json!({ "error": format!("{}", self) });
        Response::build().sized_body(Cursor::new(json.to_string()))
                         .header(ContentType::JSON)
                         .status(Status::InternalServerError)
                         .ok()
    }
}
