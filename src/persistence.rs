use tempfile::tempdir;

use crate::errors::*;
use serde::{Deserialize, Serialize};
use std::path::PathBuf;


///! The result of a crawl
#[derive(Debug, PartialEq, Clone, Serialize, Deserialize)]
pub struct CrawlResult {
    ///! The domain that was crawled
    pub domain: String,

    ///! A list of internal URL's found for this domain
    pub internal: Vec<String>,

    ///! A list of external URL's found for this domain
    pub external: Vec<String>,
}

impl CrawlResult {
    ///! The total unique URL's found in this crawlresult
    pub fn total(&self) -> usize {
        self.internal.len() + self.external.len()
    }
}

///! Wrapper around our database
#[derive(Clone)]
pub struct Persistence {
    db: sled::Db,
}


impl Persistence {
    ///! Creates a database in a temporary directory
    pub fn create_tmp() -> Result<Persistence> {
        let dir = tempdir()?;
        Ok(Self { db: sled::Db::start_default(dir)? })
    }

    ///! Creates or opens a database in the given directory
    pub fn from_path(path: &PathBuf) -> Result<Persistence> {
        Ok(Self { db: sled::Db::start_default(path)? })
    }

    ///! Stores the given CrawlResult in the database
    pub fn store(&self, item: &CrawlResult) -> Result<()> {
        let bytes: Vec<u8> = bincode::serialize(&item)?;
        self.db.set(item.domain.clone(), bytes)?;
        Ok(())
    }

    ///! Tries to find a domain in the database
    pub fn get(&self, domain: &str) -> Result<Option<CrawlResult>> {
        if let Some(bytes) = self.db.get(domain)? {
            Ok(Some(bincode::deserialize(&bytes)?))
        } else {
            Ok(None)
        }
    }

    ///! Returns a list of all the items in the database
    pub fn items(&self) -> Vec<CrawlResult> {
        self.db
            .iter()
            .flatten()
            .map(|(_domain, bytes)| bincode::deserialize(&bytes))
            .flatten()
            .collect()
    }
}


#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_create_store_get() {
        let p = Persistence::create_tmp().unwrap();

        let item = CrawlResult { domain: String::from("example.com"),
                                 internal: vec![String::from("/index.html"),
                                                String::from("/test.html")],
                                 external: Vec::new() };

        p.store(&item).expect("Should succeed");

        assert_eq!(p.get(&item.domain).expect("Should succeed"), Some(item));
    }

    #[test]
    fn test_persistence() {
        let path = tempdir().unwrap().into_path();
        let item = CrawlResult { domain: String::from("example.com"),
                                 internal: vec![String::from("/index.html"),
                                                String::from("/test.html")],
                                 external: Vec::new() };
        {
            let p = Persistence::from_path(&path).unwrap();
            p.store(&item).expect("Should succeed");
        }
        // database is closed
        {
            let p = Persistence::from_path(&path).unwrap();
            assert_eq!(p.get(&item.domain).expect("Should succeed"),
                       Some(item));
        }
    }

}
