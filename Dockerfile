FROM rustlang/rust:nightly


WORKDIR /srv/crawly/
COPY Cargo.lock Cargo.toml Rocket.toml /srv/crawly/
COPY src/* /srv/crawly/src/

RUN cargo install --path .

EXPOSE 8000

CMD ["crawly"]
