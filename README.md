# Crawly
The tiny web crawler.

## "Features"

- There is no JavaScript execution. So only static pages are supported.
- Pages are requested and processed sequentially, so it is rather slow to crawl
  a website with a lot of links. One could think of this as a kind of rate limiting.

## Usage

Install with `cargo install --force --path .`

Run with `crawly`.

## Configuration

You can configure where the database is stored in `Rocket.toml` with `database_path`.
For info about other configuration options (port, log level, ...), take a look
at https://rocket.rs/v0.4/guide/configuration/.

## Docker

You can build the container with `docker build -t crawly .`

To enable a persistent database, you can create a volume with 
`docker volume create crawlydb`

And then start the service with
```
docker run -v crawlydb:/srv/crawly/db -p8000:8000 crawly
```


## API

### `GET /`

List all currently indexed domains,

```
{
    "example.com": {
        "internal": 10,
        "external": 20,
        "total": 30
    },
    ...
}
```

### `GET /<domain>`

Fetch the crawled result of the given domain. Returns a 404 if the domain is 
not indexed (yet).

```
{
    "domain": "example.com",
    "internal": [
        "/",
        "/about/",
        ...
    ],
    "external": [
        "voorbeeld.be",
        ...
    ]
}
```

### `POST /<domain>`

Crawls the given domain. Returns the same result as `GET /<domain`. But this request can take a while to complete (until the page is fully crawled). Don't
worry if the request times out, crawly will keep crawling in the background
and the results will be available soon.
